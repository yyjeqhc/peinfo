#include "datadirs.h"
#include<QDesktopWidget>
#include<qapplication.h>
DataDirs::DataDirs(QWidget *parent)// : QWidget(parent)
{
    this->setWindowTitle(QS("����Ŀ¼��"));
    this->setWindowFlags(Qt::Widget|Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
    const int w = 800, h = 450;
    const int x = (QApplication::desktop()->width() - w) >> 1;
    const int y = (QApplication::desktop()->height() - h) >> 1;
    this->setGeometry(x, y, w, h);
    this->setMouseTracking(true);
    browser = new QTextBrowser(this);
    browser->setGeometry(0,0,w,h);
    m_directory = new IMAGE_DATA_DIRECTORY[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
}

void DataDirs::setData(IMAGE_DATA_DIRECTORY *directory)
{
    m_directory = directory;
    browser->clear();
    for(int i=0;i<IMAGE_NUMBEROF_DIRECTORY_ENTRIES;i++)
    {
//        printf("%x \n",directory[i].VirtualAddress);
        showData(m_directory[i],i);
    }
//    cout<<"setdata"<<endl;
    browser->show();
}

void DataDirs::showData(IMAGE_DATA_DIRECTORY data, int index)
{
//    printf("%x \n",data.VirtualAddress);
    QString line = tableName[index] +" VirtualAddress " + sQS(formatFourBytes((unsigned char*)&data.VirtualAddress)) + " Size " + sQS(formatFourBytes((unsigned char*)&data.Size));
    browser->append(line);
//    cout<<formatFourBytes((unsigned char*)&data.VirtualAddress)<<endl;
}

string DataDirs::formatFourBytes(unsigned char data[])
{
    string value = "0x";
    char str[10];
    sprintf(str,"%02X%02X%02X%02X",data[3],data[2],data[1],data[0]);
    value = value + str + "H";
    return value;
}
