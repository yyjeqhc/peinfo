#include "sections.h"
#include<QDesktopWidget>
#include<qapplication.h>
Sections::Sections(QWidget *parent)//:QWidget(parent)
{
    this->setWindowTitle(QS("节表信息"));
    this->setWindowFlags(Qt::Widget|Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
    const int w = 800, h = 450;
    const int x = (QApplication::desktop()->width() - w) >> 1;
    const int y = (QApplication::desktop()->height() - h) >> 1;
    this->setGeometry(x, y, w, h);
    this->setMouseTracking(true);
    browser = new QTextBrowser(this);
    browser->setGeometry(0,0,w,h);
}

void Sections::setData(IMAGE_SECTION_HEADER *header, int sectionNum)
{
    m_header = header;
    m_sectionNum = sectionNum;
    browser->clear();
    for(int i=0;i<sectionNum;i++)
    {
        showData(header[i],i);
    }
    browser->show();
}

void Sections::showData(IMAGE_SECTION_HEADER data,int index)
{
    /*
     * DWORD   SizeOfRawData;
    DWORD   PointerToRawData;
    DWORD   PointerToRelocations;
    DWORD   PointerToLinenumbers;
    WORD    NumberOfRelocations;
    WORD    NumberOfLinenumbers;
    DWORD   Characteristics;
     */
    QString line1 = QString(index) + (char*)data.Name;
    QString line2 = QS("VirtualSize(未对齐前真实长度) ") + sQS(formatFourBytes((unsigned char*)&data.Misc.VirtualSize)) + QS(" VirtualAddress(内存中的偏移) ") + sQS(formatFourBytes((unsigned char*)&data.VirtualAddress));
    QString line3 = QS("SizeOfRawData(文件对齐后的长度) ") + sQS(formatFourBytes((unsigned char*)&data.SizeOfRawData)) + QS(" PointerToRawData(文件中的偏移) ") + sQS(formatFourBytes((unsigned char*)&data.PointerToRawData));
    QString line4 = "PointerToRelocations " + sQS(formatFourBytes((unsigned char*)&data.PointerToRelocations)) + " PointerToLinenumbers " + sQS(formatFourBytes((unsigned char*)&data.PointerToLinenumbers));
    QString line5 = "NumberOfRelocations " + sQS(to_string(data.NumberOfRelocations)) + " NumberOfLinenumbers " + sQS(to_string(data.NumberOfLinenumbers));
    QString line6 = "Characteristics " + sQS(formatFourBytes((unsigned char*)&data.Characteristics));
    QString line7 = "";
//    qDebug()<<"data.Characteristics&0x40000000 = "<<(data.Characteristics&0x40000000)<<endl;
    if((data.Characteristics&0x80000000)==0x80000000)
    {
        line7 += QS("可写 ");
    }
    if((data.Characteristics&0x40000000)==0x40000000)
    {
        line7 += QS("可读 ");
    }
    if((data.Characteristics&0x20000000)==0x20000000)
    {
        line7 += QS("可执行 ");
    }
    if((data.Characteristics&0x10000000)==0x10000000)
    {
        line7 += QS("可共享 ");
    }
    if((data.Characteristics&0x8000000)==0x8000000)
    {
        line7 += QS("不到磁盘 ");
    }
    if((data.Characteristics&0x4000000)==0x4000000)
    {
        line7 += QS("不到缓存 ");
    }
    if((data.Characteristics&0x2000000)==0x2000000)
    {
        line7 += QS("重定位 ");
    }
    if((data.Characteristics&0x80)==0x80)
    {
        line7 += QS("有未初始化数据 ");
    }
    if((data.Characteristics&0x40)==0x40)
    {
        line7 += QS("有已初始化数据 ");
    }
    if((data.Characteristics&0x20)==0x20)
    {
        line7 += QS("有代码 ");
    }
    browser->append(line1);
    browser->append(line2);
    browser->append(line3);
    browser->append(line4);
    browser->append(line5);
    browser->append(line6);
    if(line7!="")
    {
        browser->append(line7);
    }
//    qDebug()<<line7<<endl;
}

string Sections::formatFourBytes(unsigned char data[4])
{
    string value = "0x";
    char str[10];
    sprintf(str,"%02X%02X%02X%02X",data[3],data[2],data[1],data[0]);
    value = value + str + "H";
    return value;
}

string Sections::formatTwoBytes(unsigned char data[])
{
    string value = "0x";
    char str[6];
    sprintf(str,"%02X%02X",data[1],data[0]);
    value = value + str + "H";
    return value;
}
