#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QFileDialog>
#include<QDragEnterEvent>
#include<QDropEvent>
#include<QMimeData>
#include<QDebug>
#include<QDir>
#include<QMessageBox>
#include<iostream>
#include<time.h>
#include<Windows.h>
#include<winnt.h>
#include<sections.h>
#include<datadirs.h>
#include<vector>
#define QS(x) QString::fromLocal8Bit((x))
#define sQS(x) QString::fromLocal8Bit((x).data())
#define stdS(x) string((x).toLocal8Bit())
using namespace std;

/*
typedef struct imageDosHeader{
    unsigned char e_magic[2];//MZ
    unsigned char noneed[0x3a];//不重要
    unsigned char e_lfanew[4];//PE头的位置
} IMAGE_DOS_HEADER;

typedef struct imageFileHeader{
    unsigned char machine[2];//运行平台
    unsigned char sectionNum[2];//节的数量
    unsigned char createtime[4];//创建时间戳
    unsigned char pointerofSymbolTable[4];//指向符号表
    unsigned char numberofSymbols[4];//符号表中符号数量
    unsigned char optionalHeaderSize[2];//可选头的大小
    unsigned char characteristics[2];//文件属性
} IMAGE_FILE_HEADER;

typedef struct dataDirectory{
    unsigned char virtualAddress[4];
    unsigned char blockSize[4];
} IMAGE_DATA_DIRECTORY;

typedef struct dataDirectories{
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_EXPORT;//导出表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_IMPORT;//导入表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_RESOURCE;//资源表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_EXCEPTION;//异常表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_SECURITY;//安全表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_BASERELOC;//重定位表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_DEBUG;//调试表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_COPYRIGHT;//版权表,保留，必须全为0
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_GLOBALPTR;//全局指针表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_TLS;//线程本地存储
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_LOADCONFIG;//加载配置表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_BOUNDIMPORT;//绑定导入表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_IAT;//IAT表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_DELAY_IMPORT;//延迟导入表
    IMAGE_DATA_DIRECTORY IMAGE_ENTRY_COM_DESCRIPTOR;//CLR表或者com库描述
    IMAGE_DATA_DIRECTORY RESERVED;//保留
} IMAGE_DATA_DIRECTORIES;

typedef struct imageOptionalHeader{
    unsigned char Magic[2];//魔术字
    unsigned char MajorLinkerVersion;//链接器主版本号
    unsigned char MinorLinkerVersion;//链接器次版本号
    unsigned char sizeOfCode[4];//所有含代码的节的总大小
    unsigned char sizeOfInitializedData[4];//所有含已初始化数据的节的总大小
    unsigned char sizeOfUninitializedData[4];//所有含未初始化数据的节的总大小
    unsigned char addressOfEntryPoint[4];//程序执行入口RVA
    unsigned char baseOfCode[4];//代码节的起始RVA
    unsigned char baseOfData[4];//数据节的起始RVA
    unsigned char imageBase[4];//建议装载地址，基址
    unsigned char sectionAlignment[4];//内存对齐大小
    unsigned char fileAlignment[4];//文件对齐大小
    unsigned char MajorOperatingSystemVersion[2];//操作系统主版本号
    unsigned char MinorOperatingSystemVersion[2];//操作系统次版本号
    unsigned char MajorImageVersion[2];//Pe主版本号
    unsigned char MinorImageVersion[2];//Pe次版本号
    unsigned char MajorSubSystemVersion[2];//子系统主版本号
    unsigned char MinorSubSystemVersion[2];//子系统次版本号
    unsigned char win32VersionValue[4];//保留未用，必须设置为0
    unsigned char sizeOfImage[4];//内存中整个PE映像大小，对齐1000H
    unsigned char sizeOfHeaders[4];//PE头+节表大小，会对齐200H
    unsigned char checkSum[4];//校验和
    unsigned char subSystem[2];//文件子系统
    unsigned char dllCharacteristic[2];//dll文件特性,针对所有pe文件
    unsigned char sizeOfStackReserve[4];//初始化时保留栈大小，默认为1M，0x100000
    unsigned char sizeOfStackCommit[4];//初始化时提交栈大小,默认为0x1000，1页
    unsigned char sizeOfHeapReverse[4];//初始化时保留堆大小,默认为1M
    unsigned char sizeOfHeapCommit[4];//初始化时提交堆大小，默认为0x1000，1页
    unsigned char loderFlags[4];//与调试相关
    unsigned char numberOfRvaAndSizes[4];//dataDirectory中的项目数量，一般为16个
    IMAGE_DATA_DIRECTORIES dataDirectory;
} IMAGE_OPTIONAL_HEADER;

typedef struct imageNtHeader{
    unsigned char signature[4];//4H
    IMAGE_FILE_HEADER fileHeader;//14H
    IMAGE_OPTIONAL_HEADER optionalHeader;//一般为0xe0,为fileheader的可选头大小
} IMAGE_NT_HEADER;

typedef struct sectionTable{
    unsigned char name[8];//节的名称
    union{
        unsigned char physicalAddress[4];
        unsigned char virtualSize[4];
    } sectionSize;//节的数据在没对齐前的尺寸，可能不准
    unsigned char virtualAddress[4];//节区的RVA
    unsigned char sizeOfRawData[4];//节在文件中对齐后的尺寸
    unsigned char pointerToRawData[4];//节区起始数据在文件的偏移
    unsigned char pointerToRelocations[4];//obj文件中，指向重定位表的指针
    unsigned char pointerToLineNums[4];//行号表的位置，调试使用
    unsigned char numberOfRelocations[2];//重定位表的个数
    unsigned char numberOfLineNums[2];//行号表中行号数量
    unsigned char characteristics[4];//节的属性
} IMAGE_SECTION_HEADER;

typedef struct importSection{
    union{
        unsigned char characteristic[4];
        unsigned char OriginalFirstThunk[4];
    } bridge1;//桥1
    unsigned char timeDateStamp[4];//时间戳
    unsigned char forwardChain[4];//链表的前一个结构
    unsigned char name1[4];//指向链接库名字的指针，是一个RVA
    unsigned char firstThunk[4];//桥2，指向从name1引入的所有导入函数
} IMAGE_IMPORT_DESCRIPTOR;*/
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    bool isPeFile(string file);
    unsigned int getRealAddr(unsigned char data[4]);
    void showPeInfo(string file);
    string formatFourBytes(unsigned char data[4]);
    string formatEightBytes(unsigned char data[8]);
    string formatTwoBytes(unsigned char data[2]);
    int getSectionTableIndex(int virtualAddress);//给定RVA，返回在第几个节区
protected:
    void dropEvent(QDropEvent *event);//拖动文件夹
    void dragEnterEvent(QDragEnterEvent *event);

private slots:
    void on_checkPe_clicked();

    void on_toolButton_clicked();

    void on_sectionTable_clicked();

    void on_dataDirectory_clicked();

private:
    Ui::Widget *ui;
    bool optionalSizeE0;
    int peHeaderSize;
    int optionalSize;
    int m_sectionNum;

    bool pe32;
    bool pe64;

    IMAGE_SECTION_HEADER* sectionTabel;
    Sections* sections;
    DataDirs* dataDirs;
    IMAGE_DATA_DIRECTORY* dataDir;
};
#endif // WIDGET_H
