#ifndef SECTIONS_H
#define SECTIONS_H

#include <QObject>
#include <QWidget>
#include <Windows.h>
#include <winnt.h>
#include <iostream>
#include <QListWidget>
#include <QListWidgetItem>
#include <QGridLayout>
#include <QTextBrowser>
#include <QDebug>
#define QS(x) QString::fromLocal8Bit((x))
#define sQS(x) QString::fromLocal8Bit((x).data())
#define stdS(x) string((x).toLocal8Bit())
using namespace std;
class Sections : public QWidget
{
    Q_OBJECT
public:
    explicit Sections(QWidget *parent = nullptr);
    void setData(IMAGE_SECTION_HEADER* header = nullptr,int sectionNum = 0);
    void showData(IMAGE_SECTION_HEADER data,int index);
    string formatFourBytes(unsigned char data[4]);
    string formatTwoBytes(unsigned char data[2]);
private:
    IMAGE_SECTION_HEADER* m_header;
    int m_sectionNum;
//    QGridLayout* mainLayout;
    QTextBrowser* browser;
signals:

};

#endif // SECTIONS_H
