#ifndef DATADIRS_H
#define DATADIRS_H

#include <QObject>
#include <QWidget>
#include <Windows.h>
#include <winnt.h>
#include <iostream>
#include <QListWidget>
#include <QListWidgetItem>
#include <QGridLayout>
#include <QTextBrowser>
#include <QDebug>
#define QS(x) QString::fromLocal8Bit((x))
#define sQS(x) QString::fromLocal8Bit((x).data())
#define stdS(x) string((x).toLocal8Bit())
using namespace std;

const static QString tableName[IMAGE_NUMBEROF_DIRECTORY_ENTRIES] = {QS("导出表"),QS("导入表"),QS("资源表"),QS("异常表"),\
                                                                    QS("安全表"),QS("重定位表"),\
                                                     QS("调试表"),QS("版权表"),QS("全局指针表"),QS("线程本地存储"),\
                                                     QS("加载配置表"),QS("绑定导入表"),QS("IAT表"),\
                                                     QS("延迟导入表"),QS("CLR表"),QS("保留未用")};

class DataDirs : public QWidget
{
    Q_OBJECT
public:
    explicit DataDirs(QWidget *parent = nullptr);
    void setData(IMAGE_DATA_DIRECTORY* directory = nullptr);
    void showData(IMAGE_DATA_DIRECTORY data,int index);
    string formatFourBytes(unsigned char data[4]);
signals:

private:
    QTextBrowser* browser;
    IMAGE_DATA_DIRECTORY* m_directory;
};

#endif // DATADIRS_H
