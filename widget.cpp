#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->setAcceptDrops(true);
    sections = new Sections(this);
    dataDirs = new DataDirs(this);
    dataDir = new IMAGE_DATA_DIRECTORY[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
}

Widget::~Widget()
{
    delete ui;
}

bool Widget::isPeFile(string fileName)
{
    FILE* file = fopen(fileName.c_str(),"rb");
    if(!file)
    {
        cout<<"打开文件失败"<<endl;
        return false;
    }
    cout<<"打开文件成功"<<endl;
    char data[100];
    memset(data,0,100);
    fread(data,1,100,file);
    IMAGE_DOS_HEADER dosheader;
    memcpy(&dosheader,data,0x40);
    unsigned char mz[2] = {0x4d,0x5a};
//    if(!(dosheader.e_magic[0]==0x4d&&dosheader.e_magic[1]==0x5a))
    if(memcmp((unsigned char*)&dosheader.e_magic,mz,2))
    {
        cout<<"mz头部错误"<<endl;
        return false;
    }
//    int peBegin = getRealAddr((unsigned char*)&dosheader.e_lfanew);
    int peBegin = dosheader.e_lfanew;
    int readLen = peBegin + 4 + 20;
    fseek(file,0,SEEK_SET);
    unsigned char *ntHeader = new unsigned char[readLen];//读取到可选头之前
    memset(ntHeader,0,readLen);
    fread(ntHeader,1,readLen,file);
    unsigned char signature[4] = {0};
    memcpy(signature,ntHeader + peBegin,4);
    unsigned char peHeader[4] = {0x50,0x45,0x0,0x0};
    if(memcmp(signature,peHeader,4))
    {
        cout<<"pe标志错误"<<endl;
        return false;
    }
    IMAGE_FILE_HEADER fileheader;
    memcpy(&fileheader,ntHeader + peBegin + 4,20);
//    cout<<(int)fileheader.machine[0]<<" "<<(int)fileheader.machine[1]<<endl;
//    unsigned char e0[2] = {0xe0,0x0};
//    if(!memcmp((unsigned char*)&fileheader.SizeOfOptionalHeader,e0,2))
//    {
//        optionalSizeE0 = true;
//        peHeaderSize = readLen + 0xe0;
//        cout<<"可选部分为E0"<<endl;
//        cout<<fileheader.SizeOfOptionalHeader<<endl;
//        return false;
//    }
    optionalSize = fileheader.SizeOfOptionalHeader;
    if(optionalSize==224)
    {
        pe32 = true;
    }
    else if(optionalSize==240)
    {
        pe64 = true;
    }

    cout<<"可选部分为 "<<optionalSize<<endl;
    peHeaderSize = readLen + optionalSize;
    free(ntHeader);
    fclose(file);
    return true;
}

/*
 *
*/
unsigned int Widget::getRealAddr(unsigned char data[4])
{
    unsigned int addr = 0;
    addr = data[0] + data[1]*256 + data[2]*65536+data[3]*16777216;
    return addr;
}

void Widget::showPeInfo(string file)
{
    FILE* fp;
    fp = fopen(file.c_str(),"rb");
    unsigned char *peHeader = new unsigned char[peHeaderSize];//到可选头为止
    fread(peHeader,1,peHeaderSize,fp);


    if(pe64)
    {
        cout<<"pe64"<<endl;
        IMAGE_NT_HEADERS64 ntHeader;
        int ntheaderLen = 0x18 + optionalSize;//pe00 + 0x14
        memcpy(&ntHeader,peHeader+peHeaderSize-ntheaderLen,ntheaderLen);

        IMAGE_FILE_HEADER fileheader = ntHeader.FileHeader;

        IMAGE_OPTIONAL_HEADER64 opheader = ntHeader.OptionalHeader;

    //    dataDir = opheader.DataDirectory;//给类成员指针赋值应该使用内存拷贝，不能直接=，超出函数范围就失效
    //    memcpy(dataDir,&ntHeader.OptionalHeader.DataDirectory,sizeof(ntHeader.OptionalHeader.DataDirectory));//2.23
        memcpy(dataDir,&opheader.DataDirectory,sizeof(opheader.DataDirectory));

        cout<<sizeof(IMAGE_OPTIONAL_HEADER64)<<endl;
//        return;
        //machine 平台 待完善
        if(true)
        {
            unsigned char machine[2] = {0x4c,0x1};
//            if(memcmp((unsigned char*)&fileheader.Machine,machine,2)==0)
//            {
//                ui->machine->setText(QS("Intel 386处理器"));
//            }
            if(fileheader.Machine==IMAGE_FILE_MACHINE_I386)
            {
                ui->machine->setText(QS("Intel 386处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_AMD64)
            {
                ui->machine->setText(QS("x64处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_ARM)
            {
                ui->machine->setText(QS("ARM小尾处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_ARMV7)
            {
                ui->machine->setText(QS("ARMv7处理器的Thumb模式"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_IA64)
            {
                ui->machine->setText(QS("Intel Itanium处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_POWERPC)
            {
                ui->machine->setText(QS("Power PC小尾处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_THUMB)
            {
                ui->machine->setText(QS("ARM或Thumb处理器"));
            }
            else
            {
                ui->machine->setText(sQS(formatTwoBytes((unsigned char*)&fileheader.Machine)) + QS(" UNKNOWN"));
            }

        }
        //区块数目
        if(true)
        {
            m_sectionNum = fileheader.NumberOfSections;//重新求
            ui->sectionNumber->setText(QString("%1").arg(m_sectionNum));
        }

        //SectionTables
        if(true)
        {
            int sectionTableSize = sizeof(IMAGE_SECTION_HEADER)*m_sectionNum;
            sectionTabel = new IMAGE_SECTION_HEADER[fileheader.NumberOfSections];
    //        memcpy(section,)
            fread(sectionTabel,1,sectionTableSize,fp);
    //        cout<<"2 "<<ftell(fp)<<endl;
        }

        //创建时间戳
        if(true)
        {
            time_t time = getRealAddr((unsigned char*)&fileheader.TimeDateStamp);
            struct tm* ttime;
            ttime = localtime(&time);
            char now[24];
            strftime(now,24,"%Y-%m-%d %H:%M:%S",ttime);
            cout<<"创建时间"<<now<<endl;
            ui->cerateTime->setText(now);
        }
        //optional header大小
        if(true)
        {
            int opheaderSize = fileheader.SizeOfOptionalHeader;
            ui->opheaderSize->setText(QString("%1 %2").arg(opheaderSize).arg(sQS(formatTwoBytes((unsigned char*)&fileheader.SizeOfOptionalHeader))));
        }
        //characteristic
        if(true)
        {
            int characteristic = fileheader.Characteristics;
            ui->fileAttr->setText(QString("%1 %2").arg(characteristic).arg(sQS(formatTwoBytes((unsigned char*)&fileheader.Characteristics))));
        }
        //pointerOfSymbolTable
        if(true)
        {
            int pointerOfSymbolTable = fileheader.PointerToSymbolTable;
            ui->symbol->setText(QString("%1").arg(pointerOfSymbolTable));
        }
        //numberOfSymbol
        if(true)
        {
            int numberOfSymbol = getRealAddr((unsigned char*)&fileheader.NumberOfSymbols);
            ui->symbolNumber->setText(QString("%1").arg(numberOfSymbol));
        }

        //magic
        if(true)
        {
            if(opheader.Magic==0x010B)
            {
                ui->magic->setText(QS("PE32"));
            }
            else if(opheader.Magic==0x0107)
            {
                ui->magic->setText(QS("ROM映像"));
            }
            else if(opheader.Magic==0x020B)
            {
                ui->magic->setText(QS("PE32+"));
            }
            else
            {
                 ui->magic->setText(QS("未知的文件类型"));
            }
        }

        //内存对齐大小
        if(true)
        {
            int sectionAlignNum = getRealAddr((unsigned char*)&opheader.SectionAlignment);
            ui->sectionAlign->setText(QString("%1").arg(sectionAlignNum));
        }
        //文件对齐大小
        if(true)
        {
            int fileAlignNum = getRealAddr((unsigned char*)&opheader.FileAlignment);
            ui->fileAlign->setText(QString("%1").arg(fileAlignNum));
        }
        //程序执行入口
        if(true)
        {
            string entryPoint = formatFourBytes((unsigned char*)&opheader.AddressOfEntryPoint);
            ui->entryPoint->setText(sQS(entryPoint));
        }
        //默认装入基址
        if(true)
        {
            string baseAddr = formatEightBytes((unsigned char*)&opheader.ImageBase);//eightBytes
            ui->baseAddress->setText(sQS(baseAddr));
        }
        //代码区块RVA
        if(true)
        {
            string baseCode = formatFourBytes((unsigned char*)&opheader.BaseOfCode);
            ui->codeRva->setText(sQS(baseCode));
        }
        //数据区块RVA
        if(true)
        {
//            string baseData = formatFourBytes((unsigned char*)&opheader.BaseOfData);
//            ui->dataRva->setText(sQS(baseData));
            ui->dataRva->setText(QS("64位PE文件没有此属性"));
        }
        //映像总尺寸
        if(true)
        {
            string imageSize = formatFourBytes((unsigned char*)&opheader.SizeOfImage);
            ui->imageSize->setText(sQS(imageSize));
        }
        //pe头部 ntheader + sectionTable
        if(true)
        {
            string headerSize = formatFourBytes((unsigned char*)&opheader.SizeOfHeaders);
            ui->peHeadSize->setText(sQS(headerSize));
        }
        //校验和
        if(true)
        {
            string checkSum = formatFourBytes((unsigned char*)&opheader.CheckSum);
            ui->checkSum->setText(sQS(checkSum));
        }
        //SubSystem
        if(true)
        {
            string subSystem = formatTwoBytes((unsigned char*)&opheader.Subsystem);
            if(opheader.Subsystem==IMAGE_SUBSYSTEM_WINDOWS_GUI)
            {
                subSystem += " Windows图形用户界面";
            }
            else if(opheader.Subsystem==IMAGE_SUBSYSTEM_WINDOWS_CUI)
            {
                subSystem += " 控制台程序";
            }
            else if(opheader.Subsystem==IMAGE_SUBSYSTEM_WINDOWS_CE_GUI)
            {
                subSystem += " Windows CE图形界面";
            }
            ui->subsystem->setText(sQS(subSystem));
        }
        //dllCharacteristic
        if(true)
        {
            string dllCharacteristic = formatTwoBytes((unsigned char*)&opheader.DllCharacteristics);
            ui->dllAttr->setText(sQS(dllCharacteristic));
        }
        //导入表
        if(true)
        {
    //        int importTableBeginAddress = getRealAddr((unsigned char*)&dataDir[1].VirtualAddress);
    //        int importTableSize = getRealAddr((unsigned char*)&dataDir[1].Size);
            int importTableBeginAddress = dataDir[1].VirtualAddress;
            int importTableSize = dataDir[1].Size;
            cout<<"importTableBeginAddress = "<<importTableBeginAddress<<endl;
            cout<<"importTableSize = "<<importTableSize<<endl;

            int index = 0;
            int virtualAddress;
            int VirtualSize;
            for(int i=0;i<m_sectionNum;i++)
            {
    //            virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //            VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                virtualAddress = sectionTabel[i].VirtualAddress;
                VirtualSize = sectionTabel[i].Misc.VirtualSize;
                cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                if(virtualAddress + VirtualSize>importTableBeginAddress)
                {
                    index = i;
                    break;
                }
            }
            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
            cout<<"fileBegin = "<<fileBegin<<endl;
            fseek(fp,sectionTabel[index].PointerToRawData,SEEK_SET);
            unsigned char *importData = new unsigned char[sectionHeader.Misc.VirtualSize];
            fread(importData,1,sectionHeader.Misc.VirtualSize,fp);
    //        for(int i=1;i<=sectionHeader.Misc.VirtualSize;i++)
    //        {
    //            printf("%x ",importData[i-1]);
    //            if(i%16==0)
    //                cout<<endl;
    //        }
    //        cout<<endl;
            int desNumber = importTableSize/20;
            cout<<"desNumber = "<<desNumber<<endl;
            cout<<"virtualaddress = "<<virtualAddress<<endl;
            IMAGE_IMPORT_DESCRIPTOR importArr[desNumber];
            memcpy(importArr,importData + (importTableBeginAddress - sectionHeader.VirtualAddress),importTableSize);
            for(int i=0;i<desNumber;i++)
            {
                cout<<formatFourBytes((unsigned char*)&importArr[i].OriginalFirstThunk)<<endl;
            }
            vector<vector<IMAGE_THUNK_DATA64>> thunkDatas;
            for(int i=0;i<desNumber-1;i++)
            {
                if(importArr[i].Name==0)
                {
                    break;
                }
                vector<IMAGE_THUNK_DATA64> thunkData;
                int thunkBegin = importArr[i].OriginalFirstThunk;
                cout<<"OriginalFirstThunk = "<<thunkBegin<<endl;
                IMAGE_THUNK_DATA64 tmpThunk;
                int offset = 0;
               do{
                    memcpy(&tmpThunk,importData + (thunkBegin - virtualAddress+offset),8);
                    if((tmpThunk.u1.AddressOfData&0x8000000000000000))
                    {
                        cout<<"OriginalFirstThunk 不是RVA"<<endl;
                        break;
                    }
                    cout<<"int = "<<tmpThunk.u1.AddressOfData<<endl;
                    cout<<formatFourBytes((unsigned char*)&tmpThunk.u1.AddressOfData)<<endl;//一般来说，后4个字节都是0
                    offset+=8;
                    if(tmpThunk.u1.AddressOfData!=0)
                    {
                        thunkData.push_back(tmpThunk);
                        unsigned short hint;
                        memcpy(&hint,importData + (tmpThunk.u1.AddressOfData - virtualAddress),2);
                        cout<<formatTwoBytes((unsigned char*)&hint)<<" hint = "<<hint<<endl;//输出导入函数编号
    //                    IMAGE_IMPORT_BY_NAME importArr;
                        cout<<(importData + (tmpThunk.u1.AddressOfData - virtualAddress+2))<<endl;//输出导入函数名称 名称紧跟标号
                    }
                } while(tmpThunk.u1.AddressOfData!=0);
                thunkDatas.push_back(thunkData);

                //dll名称处理
                cout<<(importData + (importArr[i].Name - virtualAddress))<<endl;
            }
        }

        //导出表
        if(true)
        {
            int exportTableBeginAddress = dataDir[0].VirtualAddress;
            int exportTableSize = dataDir[0].Size;
            if(exportTableBeginAddress!=0)
            {
                cout<<"exportTableBeginAddress = "<<exportTableBeginAddress<<endl;
                cout<<"exportTableSize = "<<exportTableSize<<endl;

                int index = 0;
                int virtualAddress;
                int VirtualSize;
                for(int i=0;i<m_sectionNum;i++)
                {
    //                virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //                VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                    virtualAddress = sectionTabel[i].VirtualAddress;
                    VirtualSize = sectionTabel[i].Misc.VirtualSize;
                    cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                    if(virtualAddress + VirtualSize>exportTableBeginAddress)
                    {
                        index = i;
                        break;
                    }
                }
                cout<<"index = "<<index<<endl;
                IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
                cout<<"export fileBegin = "<<fileBegin<<endl;
                cout<<"size = "<<sectionHeader.Misc.VirtualSize<<endl;

                fseek(fp,sectionTabel[index].PointerToRawData,SEEK_SET);
                unsigned char *exportData = new unsigned char[sectionHeader.Misc.VirtualSize];
                fread(exportData,1,sectionHeader.Misc.VirtualSize,fp);
    //            for(int i=0;i<sectionHeader.Misc.VirtualSize-320;i++)
    //            {
    //                printf("%x ",exportData[320+i]);
    //                if(i%16==0)
    //                {
    //                    cout<<endl;
    //                }
    //            }
    //            cout<<endl;
                IMAGE_EXPORT_DIRECTORY exportTable;
                cout<<"exportTableBeginAddress = "<<exportTableBeginAddress<<endl;
                cout<<"virtualAddress = "<<virtualAddress<<endl;

                memcpy(&exportTable,exportData + (exportTableBeginAddress - virtualAddress),sizeof(IMAGE_EXPORT_DIRECTORY));
                cout<<exportTable.Name<<endl;
                //dll名称
                cout<<exportData + (exportTable.Name - virtualAddress)<<endl;
                //nbase
                cout<<"numberOfBase = "<<exportTable.Base<<endl;
                //NumberOfFunctions
                cout<<"NumberOfFunctions = "<<exportTable.NumberOfFunctions<<endl;
                //NumberOfNames
                cout<<"NumberOfNames = "<<exportTable.NumberOfNames<<endl;
                //AddressOfFunctions
                //遍历这个位置

                //AddressOfNames
                cout<<"AddressOfNames = "<<exportTable.AddressOfNames<<endl;

                int numberOfNames = exportTable.NumberOfNames;
    //            return;
                int numberOfFunctions = exportTable.NumberOfFunctions;

                cout<<"numberOfFunctions = "<<numberOfFunctions<<endl;


                WORD bh[numberOfNames];

                int addBegin = exportTable.AddressOfNames - virtualAddress;

                memcpy(&bh,exportData + (exportTable.AddressOfNameOrdinals - virtualAddress),numberOfNames*2);

    //            for(auto i:bh)
    //            {
    //                cout<<i<<endl;
    //            }
                cout<<"addBegin = "<<addBegin<<endl;
    //            return;

                for(int i=0;i<numberOfNames;i++)//输出所有有名称的导出函数
                {
                    int tmp;
                    memcpy(&tmp,exportData + (addBegin + 4*i),4);
                    cout<<exportData + (tmp - virtualAddress)<<endl;
                }

            }
        }

        //重定位表
        if(true)
        {
            int relocBeginAddress = dataDir[5].VirtualAddress;
            int relocSize = dataDir[5].Size;
            if(relocBeginAddress!=0)
            {
                int index = 0;
                int virtualAddress;
                int VirtualSize;
                for(int i=0;i<m_sectionNum;i++)
                {
    //                virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //                VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                    virtualAddress = sectionTabel[i].VirtualAddress;
                    VirtualSize = sectionTabel[i].Misc.VirtualSize;
                    cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                    if(virtualAddress + VirtualSize>relocBeginAddress)
                    {
                        index = i;
                        break;
                    }
                }

                IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
                cout<<"export fileBegin = "<<fileBegin<<endl;
                cout<<"size = "<<sectionHeader.Misc.VirtualSize<<endl;

                fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                unsigned char *relocData = new unsigned char[sectionHeader.Misc.VirtualSize];
                fread(relocData,1,sectionHeader.Misc.VirtualSize,fp);

                int offset = relocBeginAddress - sectionHeader.VirtualAddress;
                while(sectionHeader.Misc.VirtualSize-offset>=8)
                {
                    IMAGE_BASE_RELOCATION relocBaseData;
                    memcpy(&relocBaseData,relocData + (offset),sizeof(IMAGE_BASE_RELOCATION));

                    cout<<formatFourBytes((unsigned char*)&relocBaseData.VirtualAddress)<<endl;
                    cout<<formatFourBytes((unsigned char*)&relocBaseData.SizeOfBlock)<<endl;;
                    if(relocBaseData.VirtualAddress==0)
                    {
                        break;
                    }
                    else
                    {

                        int number = relocBaseData.SizeOfBlock-8;
                        number/=2;
                        WORD relo[number];
                        memcpy(&relo,relocData + (offset+8),sizeof(relo));
                        for(int j=0;j<number;j++)
                        {
                            cout<<formatTwoBytes((unsigned char*)&relo[j])<<endl;
                            if(relo[j]==0)
                            {
                                break;
                            }
                        }
                        offset+=relocBaseData.SizeOfBlock;
                    }
//                    break;
                }
            }
        }

        //延迟导入表
        if(true)
        {
            int delayBeginAddress = dataDir[13].VirtualAddress;
            int delaySize = dataDir[13].Size;
            if(delayBeginAddress!=0)
            {
                int index = 0;
                int virtualAddress;
                int VirtualSize;
                for(int i=0;i<m_sectionNum;i++)
                {
    //                virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //                VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                    virtualAddress = sectionTabel[i].VirtualAddress;
                    VirtualSize = sectionTabel[i].Misc.VirtualSize;
                    cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                    if(virtualAddress + VirtualSize>delayBeginAddress)
                    {
                        index = i;
                        break;
                    }
                }

                IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
                cout<<"delay fileBegin = "<<fileBegin<<endl;
                cout<<"size = "<<sectionHeader.Misc.VirtualSize<<endl;
                cout<<"index = "<<index<<endl;


                fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                unsigned char *delayData = new unsigned char[sectionHeader.Misc.VirtualSize];
                fread(delayData,1,sectionHeader.Misc.VirtualSize,fp);

                int delayNumber = delaySize/sizeof(IMAGE_DELAYLOAD_DESCRIPTOR);
                delayNumber--;
                IMAGE_DELAYLOAD_DESCRIPTOR delayTables[delayNumber];
                memcpy(&delayTables,delayData + (delayBeginAddress-virtualAddress),sizeof(IMAGE_DELAYLOAD_DESCRIPTOR)*delayNumber);
                for(int i=0;i<delayNumber;i++)
//                for(int i=0;i<1;i++)
                {
                    IMAGE_DELAYLOAD_DESCRIPTOR delayTable = delayTables[i];
                    cout<<"AllAttributes "<<formatFourBytes((unsigned char*)&delayTable.Attributes.AllAttributes)<<endl;
                    cout<<"DllNameRVA "<<formatFourBytes((unsigned char*)&delayTable.DllNameRVA)<<endl;
                    cout<<"ModuleHandleRVA "<<formatFourBytes((unsigned char*)&delayTable.ModuleHandleRVA)<<endl;
                    cout<<"ImportAddressTableRVA "<<formatFourBytes((unsigned char*)&delayTable.ImportAddressTableRVA)<<endl;
                    cout<<"ImportNameTableRVA "<<formatFourBytes((unsigned char*)&delayTable.ImportNameTableRVA)<<endl;
                    cout<<"BoundImportAddressTableRVA "<<formatFourBytes((unsigned char*)&delayTable.BoundImportAddressTableRVA)<<endl;
                    cout<<"UnloadInformationTableRVA "<<formatFourBytes((unsigned char*)&delayTable.UnloadInformationTableRVA)<<endl;
                    cout<<"TimeDateStamp "<<formatFourBytes((unsigned char*)&delayTable.TimeDateStamp)<<endl;
                    //dllName
                    if(true)
                    {
                        int index = getSectionTableIndex(delayTable.DllNameRVA);
                        if(index!=-1)
                        {
                            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;


                            fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                            unsigned char *tmpData = new unsigned char[sectionHeader.Misc.VirtualSize];
                            fread(tmpData,1,sectionHeader.Misc.VirtualSize,fp);

                            cout<<"dllName = "<<tmpData + (delayTable.DllNameRVA-sectionHeader.VirtualAddress)<<endl;
                            delete[]  tmpData;
                        }
                    }
                    //ImportAddressTableRVA
                    if(true)
                    {
                        int index = getSectionTableIndex(delayTable.ImportAddressTableRVA);
                        if(index!=-1)
                        {
                            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;


                            fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                            unsigned char *tmpData = new unsigned char[sectionHeader.Misc.VirtualSize];
                            fread(tmpData,1,sectionHeader.Misc.VirtualSize,fp);

                            int offset = delayTable.ImportAddressTableRVA - sectionHeader.VirtualAddress;
                            IMAGE_THUNK_DATA64 tmpThunk;
                            do{
                                 memcpy(&tmpThunk,tmpData + offset,sizeof(IMAGE_THUNK_DATA64));

                                 offset+=8;
                                 if(tmpThunk.u1.AddressOfData&0x8000000000000000)
                                 {
                                     cout<<"first thunk为序号"<<endl;
                                     break;
                                 }
                                 if(tmpThunk.u1.AddressOfData!=0)
                                 {
                                     cout<<formatEightBytes((unsigned char*)&tmpThunk.u1.AddressOfData)<<endl;

                                 }
                             } while(tmpThunk.u1.AddressOfData!=0);

                        }
                    }



                    //ImportNameTableRVA
                    if(true)
                    {
                        int index = getSectionTableIndex(delayTable.ImportNameTableRVA);
//                        cout<<"return "<<index<<endl;
                        if(index!=-1)
                        {
                            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;


                            fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                            unsigned char *tmpData = new unsigned char[sectionHeader.Misc.VirtualSize];
                            fread(tmpData,1,sectionHeader.Misc.VirtualSize,fp);

                            int offset = delayTable.ImportNameTableRVA - sectionHeader.VirtualAddress;
                            cout<<offset<<endl;
    //                        IMAGE_THUNK_DATA32 thunkData;
    //                        memcpy(&thunkData,tmpData+(offset),sizeof(IMAGE_THUNK_DATA32));
    //                        cout<<formatFourBytes((unsigned char*)&thunkData)<<endl;
                           IMAGE_THUNK_DATA64 tmpThunk;
                           do{
                                memcpy(&tmpThunk,tmpData + offset,sizeof(IMAGE_THUNK_DATA64));

                                offset+=8;
                                if(tmpThunk.u1.AddressOfData&0x8000000000000000)
                                {
                                    cout<<"first thunk为序号"<<endl;
                                    break;
                                }
                                if(tmpThunk.u1.AddressOfData!=0)
                                {
                                    cout<<formatFourBytes((unsigned char*)&tmpThunk.u1.AddressOfData)<<endl;
                                    unsigned short hint;
                                    memcpy(&hint,tmpData + (tmpThunk.u1.AddressOfData - virtualAddress),2);
                                    cout<<formatTwoBytes((unsigned char*)&hint)<<" hint = "<<hint<<endl;//输出导入函数编号
                //                    IMAGE_IMPORT_BY_NAME importArr;
                                    cout<<(tmpData + (tmpThunk.u1.AddressOfData - virtualAddress+2))<<endl;//输出导入函数名称 名称紧跟标号
                                }
                            } while(tmpThunk.u1.AddressOfData!=0);
                        }
                    }

                }
            }
        }


        //资源表
        if(true)
        {

        }
        fclose(fp);
    }
    else
    {
        if(!pe32)
        {
            QMessageBox::information(NULL,QS("提示"),QS("不是标准的可选头，默认为32位PE文件"),QMessageBox::Ok);
        }

        IMAGE_NT_HEADERS32 ntHeader;
        int ntheaderLen = 0x18 + optionalSize;//pe00 + 0x14
        memcpy(&ntHeader,peHeader+peHeaderSize-ntheaderLen,ntheaderLen);

        IMAGE_FILE_HEADER fileheader = ntHeader.FileHeader;

        IMAGE_OPTIONAL_HEADER32 opheader = ntHeader.OptionalHeader;

    //    dataDir = opheader.DataDirectory;//给类成员指针赋值应该使用内存拷贝，不能直接=，超出函数范围就失效
    //    memcpy(dataDir,&ntHeader.OptionalHeader.DataDirectory,sizeof(ntHeader.OptionalHeader.DataDirectory));//2.23
        memcpy(dataDir,&opheader.DataDirectory,sizeof(opheader.DataDirectory));

        //machine 平台 待完善
        if(true)
        {
            unsigned char machine[2] = {0x4c,0x1};
//            if(memcmp((unsigned char*)&fileheader.Machine,machine,2)==0)
//            {
//                ui->machine->setText(QS("Intel 386处理器"));
//            }
            if(fileheader.Machine==IMAGE_FILE_MACHINE_I386)
            {
                ui->machine->setText(QS("Intel 386处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_AMD64)
            {
                ui->machine->setText(QS("x64处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_ARM)
            {
                ui->machine->setText(QS("ARM小尾处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_ARMV7)
            {
                ui->machine->setText(QS("ARMv7处理器的Thumb模式"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_IA64)
            {
                ui->machine->setText(QS("Intel Itanium处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_POWERPC)
            {
                ui->machine->setText(QS("Power PC小尾处理器"));
            }
            else if(fileheader.Machine==IMAGE_FILE_MACHINE_THUMB)
            {
                ui->machine->setText(QS("ARM或Thumb处理器"));
            }
            else
            {
                ui->machine->setText(sQS(formatTwoBytes((unsigned char*)&fileheader.Machine)) + QS(" UNKNOWN"));
            }

        }
        //区块数目
        if(true)
        {
            m_sectionNum = fileheader.NumberOfSections;//重新求
            ui->sectionNumber->setText(QString("%1").arg(m_sectionNum));
        }

        //SectionTables
        if(true)
        {
            int sectionTableSize = sizeof(IMAGE_SECTION_HEADER)*m_sectionNum;
            sectionTabel = new IMAGE_SECTION_HEADER[fileheader.NumberOfSections];
    //        memcpy(section,)
            fread(sectionTabel,1,sectionTableSize,fp);
    //        cout<<"2 "<<ftell(fp)<<endl;
        }

        //创建时间戳
        if(true)
        {
            time_t time = getRealAddr((unsigned char*)&fileheader.TimeDateStamp);
            struct tm* ttime;
            ttime = localtime(&time);
            char now[24];
            strftime(now,24,"%Y-%m-%d %H:%M:%S",ttime);
            cout<<"创建时间"<<now<<endl;
            ui->cerateTime->setText(now);
        }
        //optional header大小
        if(true)
        {
            int opheaderSize = fileheader.SizeOfOptionalHeader;
            ui->opheaderSize->setText(QString("%1 %2").arg(opheaderSize).arg(sQS(formatTwoBytes((unsigned char*)&fileheader.SizeOfOptionalHeader))));
        }
        //characteristic
        if(true)
        {
            int characteristic = fileheader.Characteristics;
            ui->fileAttr->setText(QString("%1 %2").arg(characteristic).arg(sQS(formatTwoBytes((unsigned char*)&fileheader.Characteristics))));
        }
        //pointerOfSymbolTable
        if(true)
        {
            int pointerOfSymbolTable = fileheader.PointerToSymbolTable;
            ui->symbol->setText(QString("%1").arg(pointerOfSymbolTable));
        }
        //numberOfSymbol
        if(true)
        {
            int numberOfSymbol = getRealAddr((unsigned char*)&fileheader.NumberOfSymbols);
            ui->symbolNumber->setText(QString("%1").arg(numberOfSymbol));
        }

        //magic
        if(true)
        {
            if(opheader.Magic==0x010B)
            {
                ui->magic->setText(QS("PE32"));
            }
            else if(opheader.Magic==0x0107)
            {
                ui->magic->setText(QS("ROM映像"));
            }
            else if(opheader.Magic==0x020B)
            {
                ui->magic->setText(QS("PE32+"));
            }
            else
            {
                 ui->magic->setText(QS("未知的文件类型"));
            }
        }

        //内存对齐大小
        if(true)
        {
            int sectionAlignNum = getRealAddr((unsigned char*)&opheader.SectionAlignment);
            ui->sectionAlign->setText(QString("%1").arg(sectionAlignNum));
        }
        //文件对齐大小
        if(true)
        {
            int fileAlignNum = getRealAddr((unsigned char*)&opheader.FileAlignment);
            ui->fileAlign->setText(QString("%1").arg(fileAlignNum));
        }
        //程序执行入口
        if(true)
        {
            string entryPoint = formatFourBytes((unsigned char*)&opheader.AddressOfEntryPoint);
            ui->entryPoint->setText(sQS(entryPoint));
        }
        //默认装入基址
        if(true)
        {
            string baseAddr = formatFourBytes((unsigned char*)&opheader.ImageBase);
            ui->baseAddress->setText(sQS(baseAddr));
        }
        //代码区块RVA
        if(true)
        {
            string baseCode = formatFourBytes((unsigned char*)&opheader.BaseOfCode);
            ui->codeRva->setText(sQS(baseCode));
        }
        //数据区块RVA
        if(true)
        {
            string baseData = formatFourBytes((unsigned char*)&opheader.BaseOfData);
            ui->dataRva->setText(sQS(baseData));
        }
        //映像总尺寸
        if(true)
        {
            string imageSize = formatFourBytes((unsigned char*)&opheader.SizeOfImage);
            ui->imageSize->setText(sQS(imageSize));
        }
        //pe头部 ntheader + sectionTable
        if(true)
        {
            string headerSize = formatFourBytes((unsigned char*)&opheader.SizeOfHeaders);
            ui->peHeadSize->setText(sQS(headerSize));
        }
        //校验和
        if(true)
        {
            string checkSum = formatFourBytes((unsigned char*)&opheader.CheckSum);
            ui->checkSum->setText(sQS(checkSum));
        }
        //SubSystem
        if(true)
        {
            string subSystem = formatTwoBytes((unsigned char*)&opheader.Subsystem);
            if(opheader.Subsystem==IMAGE_SUBSYSTEM_WINDOWS_GUI)
            {
                subSystem += " Windows图形用户界面";
            }
            else if(opheader.Subsystem==IMAGE_SUBSYSTEM_WINDOWS_CUI)
            {
                subSystem += " 控制台程序";
            }
            else if(opheader.Subsystem==IMAGE_SUBSYSTEM_WINDOWS_CE_GUI)
            {
                subSystem += " Windows CE图形界面";
            }
            ui->subsystem->setText(sQS(subSystem));
        }
        //dllCharacteristic
        if(true)
        {
            string dllCharacteristic = formatTwoBytes((unsigned char*)&opheader.DllCharacteristics);
            ui->dllAttr->setText(sQS(dllCharacteristic));
        }

        //导入表
        if(true)
        {
    //        int importTableBeginAddress = getRealAddr((unsigned char*)&dataDir[1].VirtualAddress);
    //        int importTableSize = getRealAddr((unsigned char*)&dataDir[1].Size);
            int importTableBeginAddress = dataDir[1].VirtualAddress;
            int importTableSize = dataDir[1].Size;
            cout<<"importTableBeginAddress = "<<importTableBeginAddress<<endl;
            cout<<"importTableSize = "<<importTableSize<<endl;

            int index = 0;
            int virtualAddress;
            int VirtualSize;
            for(int i=0;i<m_sectionNum;i++)
            {
    //            virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //            VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                virtualAddress = sectionTabel[i].VirtualAddress;
                VirtualSize = sectionTabel[i].Misc.VirtualSize;
                cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                if(virtualAddress + VirtualSize>importTableBeginAddress)
                {
                    index = i;
                    break;
                }
            }
            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
            cout<<"fileBegin = "<<fileBegin<<endl;
            fseek(fp,sectionTabel[index].PointerToRawData,SEEK_SET);
            unsigned char *importData = new unsigned char[sectionHeader.Misc.VirtualSize];
            fread(importData,1,sectionHeader.Misc.VirtualSize,fp);
    //        for(int i=1;i<=sectionHeader.Misc.VirtualSize;i++)
    //        {
    //            printf("%x ",importData[i-1]);
    //            if(i%16==0)
    //                cout<<endl;
    //        }
    //        cout<<endl;
            int desNumber = importTableSize/20;
            cout<<"desNumber = "<<desNumber<<endl;
            cout<<"virtualaddress = "<<virtualAddress<<endl;
            IMAGE_IMPORT_DESCRIPTOR importArr[desNumber];
            memcpy(importArr,importData + (importTableBeginAddress - sectionHeader.VirtualAddress),importTableSize);
            for(int i=0;i<desNumber;i++)
            {
                cout<<formatFourBytes((unsigned char*)&importArr[i].OriginalFirstThunk)<<endl;
            }
            vector<vector<IMAGE_THUNK_DATA32>> thunkDatas;
            for(int i=0;i<desNumber-1;i++)
            {
                if(importArr[i].Name==0)
                {
                    break;
                }
                vector<IMAGE_THUNK_DATA32> thunkData;
                int thunkBegin = importArr[i].OriginalFirstThunk;
                cout<<"OriginalFirstThunk = "<<thunkBegin<<endl;
                IMAGE_THUNK_DATA32 tmpThunk;
                int offset = 0;
               do{
                    memcpy(&tmpThunk,importData + (thunkBegin - virtualAddress+offset),4);
                    cout<<formatFourBytes((unsigned char*)&tmpThunk.u1.AddressOfData)<<endl;
                    offset+=4;
                    if((tmpThunk.u1.AddressOfData&0x80000000))
                    {
                        cout<<"OriginalFirstThunk 不是RVA"<<endl;
                        break;
                    }
                    if(tmpThunk.u1.AddressOfData!=0)
                    {
                        thunkData.push_back(tmpThunk);
                        unsigned short hint;
                        memcpy(&hint,importData + (tmpThunk.u1.AddressOfData - virtualAddress),2);
                        cout<<formatTwoBytes((unsigned char*)&hint)<<" hint = "<<hint<<endl;//输出导入函数编号
    //                    IMAGE_IMPORT_BY_NAME importArr;
                        cout<<(importData + (tmpThunk.u1.AddressOfData - virtualAddress+2))<<endl;//输出导入函数名称 名称紧跟标号
                    }
                } while(tmpThunk.u1.AddressOfData!=0);
                thunkDatas.push_back(thunkData);

                //dll名称处理
                cout<<(importData + (importArr[i].Name - virtualAddress))<<endl;
            }
        }

        //导出表
        if(true)
        {
            int exportTableBeginAddress = dataDir[0].VirtualAddress;
            int exportTableSize = dataDir[0].Size;
            if(exportTableBeginAddress!=0)
            {
                cout<<"exportTableBeginAddress = "<<exportTableBeginAddress<<endl;
                cout<<"exportTableSize = "<<exportTableSize<<endl;

                int index = 0;
                int virtualAddress;
                int VirtualSize;
                for(int i=0;i<m_sectionNum;i++)
                {
    //                virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //                VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                    virtualAddress = sectionTabel[i].VirtualAddress;
                    VirtualSize = sectionTabel[i].Misc.VirtualSize;
                    cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                    if(virtualAddress + VirtualSize>exportTableBeginAddress)
                    {
                        index = i;
                        break;
                    }
                }
                cout<<"index = "<<index<<endl;
                IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
                cout<<"export fileBegin = "<<fileBegin<<endl;
                cout<<"size = "<<sectionHeader.Misc.VirtualSize<<endl;

                fseek(fp,sectionTabel[index].PointerToRawData,SEEK_SET);
                unsigned char *exportData = new unsigned char[sectionHeader.Misc.VirtualSize];
                fread(exportData,1,sectionHeader.Misc.VirtualSize,fp);
    //            for(int i=0;i<sectionHeader.Misc.VirtualSize-320;i++)
    //            {
    //                printf("%x ",exportData[320+i]);
    //                if(i%16==0)
    //                {
    //                    cout<<endl;
    //                }
    //            }
    //            cout<<endl;
                IMAGE_EXPORT_DIRECTORY exportTable;
                cout<<"exportTableBeginAddress = "<<exportTableBeginAddress<<endl;
                cout<<"virtualAddress = "<<virtualAddress<<endl;

                memcpy(&exportTable,exportData + (exportTableBeginAddress - virtualAddress),sizeof(IMAGE_EXPORT_DIRECTORY));
                cout<<exportTable.Name<<endl;
                //dll名称
                cout<<exportData + (exportTable.Name - virtualAddress)<<endl;
                //nbase
                cout<<"numberOfBase = "<<exportTable.Base<<endl;
                //NumberOfFunctions
                cout<<"NumberOfFunctions = "<<exportTable.NumberOfFunctions<<endl;
                //NumberOfNames
                cout<<"NumberOfNames = "<<exportTable.NumberOfNames<<endl;
                //AddressOfFunctions
                //遍历这个位置

                //AddressOfNames
                cout<<"AddressOfNames = "<<exportTable.AddressOfNames<<endl;

                int numberOfNames = exportTable.NumberOfNames;
    //            return;
                int numberOfFunctions = exportTable.NumberOfFunctions;

                cout<<"numberOfFunctions = "<<numberOfFunctions<<endl;


                WORD bh[numberOfNames];

                int addBegin = exportTable.AddressOfNames - virtualAddress;

                memcpy(&bh,exportData + (exportTable.AddressOfNameOrdinals - virtualAddress),numberOfNames*2);

    //            for(auto i:bh)
    //            {
    //                cout<<i<<endl;
    //            }
                cout<<"addBegin = "<<addBegin<<endl;
    //            return;

                for(int i=0;i<numberOfNames;i++)//输出所有有名称的导出函数
                {
                    int tmp;
                    memcpy(&tmp,exportData + (addBegin + 4*i),4);
                    cout<<exportData + (tmp - virtualAddress)<<endl;
                }

            }
        }

        //重定位表
        if(true)
        {
            int relocBeginAddress = dataDir[5].VirtualAddress;
            int relocSize = dataDir[5].Size;
            if(relocBeginAddress!=0)
            {
                int index = 0;
                int virtualAddress;
                int VirtualSize;
                for(int i=0;i<m_sectionNum;i++)
                {
    //                virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //                VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                    virtualAddress = sectionTabel[i].VirtualAddress;
                    VirtualSize = sectionTabel[i].Misc.VirtualSize;
                    cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                    if(virtualAddress + VirtualSize>relocBeginAddress)
                    {
                        index = i;
                        break;
                    }
                }

                IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
                cout<<"export fileBegin = "<<fileBegin<<endl;
                cout<<"size = "<<sectionHeader.Misc.VirtualSize<<endl;

                fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                unsigned char *relocData = new unsigned char[sectionHeader.Misc.VirtualSize];
                fread(relocData,1,sectionHeader.Misc.VirtualSize,fp);

                int offset = relocBeginAddress - sectionHeader.VirtualAddress;
                while(sectionHeader.Misc.VirtualSize-offset>=8)
                {
                    IMAGE_BASE_RELOCATION relocBaseData;
                    memcpy(&relocBaseData,relocData + (offset),sizeof(IMAGE_BASE_RELOCATION));

                    cout<<formatFourBytes((unsigned char*)&relocBaseData.VirtualAddress)<<endl;
                    cout<<formatFourBytes((unsigned char*)&relocBaseData.SizeOfBlock)<<endl;;
                    if(relocBaseData.VirtualAddress==0)
                    {
                        break;
                    }
                    else
                    {

                        int number = relocBaseData.SizeOfBlock-8;
                        number/=2;
                        WORD relo[number];
                        memcpy(&relo,relocData + (offset+8),sizeof(relo));
                        for(int j=0;j<number;j++)
                        {
                            cout<<formatTwoBytes((unsigned char*)&relo[j])<<endl;
                            if(relo[j]==0)
                            {
                                break;
                            }
                        }
                        offset+=relocBaseData.SizeOfBlock;
                    }
//                    break;
                }
            }
        }

        //延迟导入表
        if(true)
        {
            int delayBeginAddress = dataDir[13].VirtualAddress;
            int delaySize = dataDir[13].Size;
            if(delayBeginAddress!=0)
            {
                int index = 0;
                int virtualAddress;
                int VirtualSize;
                for(int i=0;i<m_sectionNum;i++)
                {
    //                virtualAddress = getRealAddr((unsigned char*)&sectionTabel[i].VirtualAddress);
    //                VirtualSize = getRealAddr((unsigned char*)&sectionTabel[i].Misc.VirtualSize);
                    virtualAddress = sectionTabel[i].VirtualAddress;
                    VirtualSize = sectionTabel[i].Misc.VirtualSize;
                    cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
                    if(virtualAddress + VirtualSize>delayBeginAddress)
                    {
                        index = i;
                        break;
                    }
                }

                IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;
                cout<<"delay fileBegin = "<<fileBegin<<endl;
                cout<<"size = "<<sectionHeader.Misc.VirtualSize<<endl;
                cout<<"index = "<<index<<endl;


                fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                unsigned char *delayData = new unsigned char[sectionHeader.Misc.VirtualSize];
                fread(delayData,1,sectionHeader.Misc.VirtualSize,fp);

                int delayNumber = delaySize/sizeof(IMAGE_DELAYLOAD_DESCRIPTOR);
                delayNumber--;
                IMAGE_DELAYLOAD_DESCRIPTOR delayTables[delayNumber];
                memcpy(&delayTables,delayData + (delayBeginAddress-virtualAddress),sizeof(IMAGE_DELAYLOAD_DESCRIPTOR)*delayNumber);
                for(int i=0;i<delayNumber;i++)
//                for(int i=0;i<1;i++)
                {
                    IMAGE_DELAYLOAD_DESCRIPTOR delayTable = delayTables[i];
                    cout<<"AllAttributes "<<formatFourBytes((unsigned char*)&delayTable.Attributes.AllAttributes)<<endl;
                    cout<<"DllNameRVA "<<formatFourBytes((unsigned char*)&delayTable.DllNameRVA)<<endl;
                    cout<<"ModuleHandleRVA "<<formatFourBytes((unsigned char*)&delayTable.ModuleHandleRVA)<<endl;
                    cout<<"ImportAddressTableRVA "<<formatFourBytes((unsigned char*)&delayTable.ImportAddressTableRVA)<<endl;
                    cout<<"ImportNameTableRVA "<<formatFourBytes((unsigned char*)&delayTable.ImportNameTableRVA)<<endl;
                    cout<<"BoundImportAddressTableRVA "<<formatFourBytes((unsigned char*)&delayTable.BoundImportAddressTableRVA)<<endl;
                    cout<<"UnloadInformationTableRVA "<<formatFourBytes((unsigned char*)&delayTable.UnloadInformationTableRVA)<<endl;
                    cout<<"TimeDateStamp "<<formatFourBytes((unsigned char*)&delayTable.TimeDateStamp)<<endl;
                    //dllName
                    if(true)
                    {
                        int index = getSectionTableIndex(delayTable.DllNameRVA);
                        if(index!=-1)
                        {
                            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;


                            fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                            unsigned char *tmpData = new unsigned char[sectionHeader.Misc.VirtualSize];
                            fread(tmpData,1,sectionHeader.Misc.VirtualSize,fp);

                            cout<<"dllName = "<<tmpData + (delayTable.DllNameRVA-sectionHeader.VirtualAddress)<<endl;
                            delete[]  tmpData;
                        }
                    }
                    //ImportAddressTableRVA
                    if(true)
                    {
                        int index = getSectionTableIndex(delayTable.ImportAddressTableRVA);
                        if(index!=-1)
                        {
                            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;


                            fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                            unsigned char *tmpData = new unsigned char[sectionHeader.Misc.VirtualSize];
                            fread(tmpData,1,sectionHeader.Misc.VirtualSize,fp);

                            int offset = delayTable.ImportAddressTableRVA - sectionHeader.VirtualAddress;
                            IMAGE_THUNK_DATA32 tmpThunk;
                            do{
                                 memcpy(&tmpThunk,tmpData + offset,sizeof(IMAGE_THUNK_DATA32));

                                 offset+=4;
                                 if(tmpThunk.u1.AddressOfData&0x80000000)
                                 {
                                     cout<<"first thunk为序号"<<endl;
                                     break;
                                 }
                                 if(tmpThunk.u1.AddressOfData!=0)
                                 {
                                     cout<<formatFourBytes((unsigned char*)&tmpThunk.u1.AddressOfData)<<endl;

                                 }
                             } while(tmpThunk.u1.AddressOfData!=0);

                        }
                    }

                    //ImportNameTableRVA
                    if(true)
                    {
                        int index = getSectionTableIndex(delayTable.ImportNameTableRVA);
//                        cout<<"return "<<index<<endl;
                        if(index!=-1)
                        {
                            IMAGE_SECTION_HEADER sectionHeader = sectionTabel[index];
                            int fileBegin = sectionHeader.PointerToRawData;//其实不用自己手动getRealAddr;;;


                            fseek(fp,sectionHeader.PointerToRawData,SEEK_SET);
                            unsigned char *tmpData = new unsigned char[sectionHeader.Misc.VirtualSize];
                            fread(tmpData,1,sectionHeader.Misc.VirtualSize,fp);

                            int offset = delayTable.ImportNameTableRVA - sectionHeader.VirtualAddress;
                            cout<<offset<<endl;
    //                        IMAGE_THUNK_DATA32 thunkData;
    //                        memcpy(&thunkData,tmpData+(offset),sizeof(IMAGE_THUNK_DATA32));
    //                        cout<<formatFourBytes((unsigned char*)&thunkData)<<endl;
                           IMAGE_THUNK_DATA32 tmpThunk;
                           do{
                                memcpy(&tmpThunk,tmpData + offset,sizeof(IMAGE_THUNK_DATA32));

                                offset+=4;
                                if(tmpThunk.u1.AddressOfData&0x80000000)
                                {
                                    cout<<"first thunk为序号"<<endl;
                                    break;
                                }
                                if(tmpThunk.u1.AddressOfData!=0)
                                {
                                    cout<<formatFourBytes((unsigned char*)&tmpThunk.u1.AddressOfData)<<endl;
                                    unsigned short hint;
                                    memcpy(&hint,tmpData + (tmpThunk.u1.AddressOfData - virtualAddress),2);
                                    cout<<formatTwoBytes((unsigned char*)&hint)<<" hint = "<<hint<<endl;//输出导入函数编号
                //                    IMAGE_IMPORT_BY_NAME importArr;
                                    cout<<(tmpData + (tmpThunk.u1.AddressOfData - virtualAddress+2))<<endl;//输出导入函数名称 名称紧跟标号
                                }
                            } while(tmpThunk.u1.AddressOfData!=0);
                        }
                    }

                }
            }
        }

        fclose(fp);

    }

}

int Widget::getSectionTableIndex(int OriginalvirtualAddress)
{
    int index = -1;
    int virtualAddress;
    int VirtualSize;
    for(int i=0;i<m_sectionNum;i++)
    {
        virtualAddress = sectionTabel[i].VirtualAddress;
        VirtualSize = sectionTabel[i].Misc.VirtualSize;
//        cout<<"i = "<<i<<" virtualAddress = "<<virtualAddress<<" VirtualSize = "<<VirtualSize<<endl;
        if(virtualAddress + VirtualSize>OriginalvirtualAddress)
        {
            index = i;
            break;
        }
    }
    return index;
}

string Widget::formatFourBytes(unsigned char data[4])
{
    string value = "0x";
    char str[10];
    sprintf(str,"%02X%02X%02X%02X",data[3],data[2],data[1],data[0]);
    value = value + str + "H";
    return value;
}

string Widget::formatEightBytes(unsigned char data[8])
{
    string value = "0x";
    char str[20];
    sprintf(str,"%02X%02X%02X%02X%02X%02X%02X%02X",data[7],data[6],data[5],data[4],data[3],data[2],data[1],data[0]);
    value = value + str + "H";
    return value;
}

string Widget::formatTwoBytes(unsigned char data[])
{
    string value = "0x";
    char str[6];
    sprintf(str,"%02X%02X",data[1],data[0]);
    value = value + str + "H";
    return value;
}



//拖动文件夹的处理
void Widget::dropEvent(QDropEvent *event)
{
//    qDebug()<<event->mimeData()->urls().size();
    QString path = event->mimeData()->urls().first().toString();
    path = path.mid(8);//去掉前面的file://
    qDebug()<<"path = "<<path<<endl;
    ui->fileEdit->setText(path);
}

void Widget::dragEnterEvent(QDragEnterEvent *event)
{
//    qDebug()<<"enter "<<event->source();
    event->acceptProposedAction();
}

void Widget::on_checkPe_clicked()
{
    pe32 = false;
    pe64 = false;
    peHeaderSize = 0;
    m_sectionNum = 0;
    optionalSize = 0;
    QString path = ui->fileEdit->text();
    if(path.isEmpty())
    {
        return;
    }
    cout<<"打开文件"<<endl;
    if(isPeFile(stdS(path)))
    {
        cout<<"该文件为PE文件"<<endl;
        showPeInfo(stdS(path));
    }
}

void Widget::on_toolButton_clicked()
{
    QString path = QFileDialog::getOpenFileName(this,"choose file.","/");
    if(!path.isEmpty())
        ui->fileEdit->setText(path);
    qDebug()<<"选择打开文件 "<<path;
}

void Widget::on_sectionTable_clicked()
{
    cout<<"hello world"<<endl;
    cout<<optionalSizeE0<<endl;
//    if(!optionalSizeE0)
//    {
//        return;
//    }
    cout<<"输出节表"<<endl;
    sections->show();
    sections->setData(sectionTabel,m_sectionNum);
}

void Widget::on_dataDirectory_clicked()
{
    cout<<"print datadirectory"<<endl;
    cout<<optionalSizeE0<<endl;
//    if(!optionalSizeE0)
//    {
//        return;
//    }
    cout<<"输出数据目录表"<<endl;
    dataDirs->show();
//    for(int i=0;i<16;i++)
//    {
//        printf("%x %x\n",dataDir[i].VirtualAddress,dataDir[i].Size);
//    }
//    cout<<"temp"<<endl;
    dataDirs->setData(dataDir);

}
